export function defaults(obj, defaultobj) {

    for (let key in defaultobj) {
        if (obj.hasOwnProperty(key) === false) {
            obj[key] = defaultobj[key];
        }
    }

    return obj;
}