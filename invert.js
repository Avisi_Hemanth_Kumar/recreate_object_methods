export function invert(obj) {

    for (let key in obj) {
        obj[obj[key]] = key;
        delete obj[key];
    }

    return obj;
}