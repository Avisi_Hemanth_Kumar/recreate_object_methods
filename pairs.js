export function pairs(obj) {
    const pairs = [];
    for (let key in obj) {
        pairs.push([key, obj[key]]);
    }
    return pairs;
}