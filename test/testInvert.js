import {
    invert
} from '/home/hemanth/MountBlue/objects/invert.js';

const testObject = {
    name: 'Bruce Wayne',
    age: 36,
    location: 'Gotham'
};

const actual = invert(testObject);

console.log(actual);