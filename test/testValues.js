import {
    values
} from '/home/hemanth/MountBlue/objects/values.js';

function testValues(actual, expected) {
    if (JSON.stringify(actual) === JSON.stringify(expected)) {
        return "PASS";
    }
    return "FAIL";
}

const testObject = {
    name: 'Bruce Wayne',
    age: 36,
    location: 'Gotham'
};

const actual = values(testObject);

const expected = Object.values(testObject);

console.log(actual + '\n');
console.log(expected + '\n');
console.log(testValues(actual, expected));