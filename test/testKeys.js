import {
    keys
} from '/home/hemanth/MountBlue/objects/keys.js';

function testKeys(actual, expected) {
    if (JSON.stringify(actual) === JSON.stringify(expected)) {
        return "PASS";
    }
    return "FAIL";
}

const testObject = {
    name: 'Bruce Wayne',
    age: 36,
    location: 'Gotham'
};

const actual = keys(testObject);

const expected = Object.keys(testObject);

console.log(actual + '\n');
console.log(expected + '\n');
console.log(testKeys(actual, expected));