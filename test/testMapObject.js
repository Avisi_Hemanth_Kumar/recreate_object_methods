import {
    mapObject
} from '/home/hemanth/MountBlue/objects/mapObject.js';

const testObject = {
    name: 'Bruce Wayne',
    age: 36,
    location: 'Gotham'
};

const actual = mapObject(testObject, function (value) {
    return value + 'g5';
});

console.log(actual);